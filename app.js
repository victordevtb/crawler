let express = require("express");
let proxy = require("./proxy.js");
let app = express();


app.get("/",async (req, res) => {
    let data = req.query.url;
    let content = await proxy(data);
    res.send(content);
});

app.listen(7070,() => {});