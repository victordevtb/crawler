const puppeteer = require('puppeteer');


async function preparePageForTests(page){
  const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
    'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
  await page.setUserAgent(userAgent);

  // Basicamente aqui estamos sobrescrevendo o objeto webdriver que indica que isso aqui é um headless chrome como falso através de uma injeção de Javascript
  await page.evaluateOnNewDocument(() => {
    Object.defineProperty(navigator, 'webdriver', {
      get: () => false,
    });
  });

  // Aqui estamos criando um objeto vazio para se passar por um chrome comum.
  await page.evaluateOnNewDocument(() => {
    // Isso aqui pode ser mais detalhado para passar em sites agressivos contra crawlers, porém por hora está passando de boa pelo site da adidas.
    window.navigator.chrome = {
      runtime: {},
      // etc.
    };
  });

  // Isso aqui é uma pegadinha que alguns anti-crawlers fazem, eles perguntam se o usuário permite notificações e se sim, eles checam se o navegador tem permissão para aceitar notificação, o que não é o caso do headless, então iria ocorrer uma contradição, e por isso fazmos um bypass
  await page.evaluateOnNewDocument(() => {
    const originalQuery = window.navigator.permissions.query;
    return window.navigator.permissions.query = (parameters) => (
      parameters.name === 'notifications' ?
        Promise.resolve({ state: Notification.permission }) :
        originalQuery(parameters)
    );
  });

  await page.evaluateOnNewDocument(() => {
    Object.defineProperty(navigator, 'plugins', {
      get: () => [1, 2, 3, 4, 5],
    });
  });

  await page.evaluateOnNewDocument(() => {
    Object.defineProperty(navigator, 'languages', {
      get: () => ['en-US', 'en'],
    });
  });
}


async function requestUrl(url, takeScreenshot = false){
    const browser = await puppeteer.launch({
      executablePath: '/usr/bin/google-chrome',
      args: ['--no-sandbox'],
      headless: true,
    });
    const page = await browser.newPage();
  
    await preparePageForTests(page);
  
    await page.goto(url);

    if(takeScreenshot){
      await page.screenshot({path: 'headless-test-result.png'});
    }
    
    let content = await page.content()
  
    await browser.close()

    return content;
}

module.exports = requestUrl;